# JDBC und DAO

## Spaß mit JDBC und DAO Einführung

Java Database Connectiviti (JDBC) und damit mit Java auf eine Relationaledatenbank (MySQL) zugreift. An Hand des DAO-Patterns (Data Access Object), womit wir den Zugriff auf die Datenbank allgemeinern kann.

Als Domaine verwenden wir ein Kurssystem (Kurse eintragen).

## DAO-Pattern

- Data Access Object (Datenzugriffsobjekt)
- Zugriff auf unterschiedliche Arten von Datenquellen so kapselt, dass die angesprochene Datenquelle ausgetauscht werden kann, ohne dass der aufgerufene Code geändert werden muss.

### Eigenschaften

- DAOs abstrahieren den Zugriff auf Datenbanken nicht vollständig, da sie nicht für die Transformation der Daten in die Struktur der Datenbank verantwortlich sind.
- DAOs sind für ein spezielles Speichermedium optimiert. Der Zugriff auf dieses Medium wird über das vom DAO vorgegebene bzw. zu implementierende APO vorgenommen
- DAOs minimieren den Portierungsaufwand einer Anwendung beim Wechsel des Speichermediums.

Eng Verwandt mit dem Entwurfsmuster Transferobjekt.

Wenn ich Daten aus der Datenbank hole bekomme ich Zeilen, diese werden anschließend konvertiert in ein für den Aplikationsentwickler gewünschtes Format.

Entsprechendes Subjekt für eine entsprechende Klasse.

Für die implementierung verwenden wir wieder MySQL mit PHP my Admin als Administrationsoberfläche (XAMPP) und Java (InteliJ)

---

## Projektsetup

⇒ Wir verwenden wieder XAMPP und starten Apache und MySQL

für die Administration PHPMyAdmin

⇒ neue Datenbank erstellen

- Datenbank Name “kurssystem”
- Name Tabelle “courses” 7 Spalten

Erstellen der Spalten der Tabelle course:

![Untitled](src/main/java/img/JDBC%20und%20D%20f1070/Untitled.png)

Einfügen eines Datensatzes:

```sql
INSERT INTO `courses` (`id`, `name`, `description`, `hours`, `begindate`,
`enddate`, `coursetype`) VALUES (NULL,
'Programmieren 1', 'Dieser Kurs beschäftigt sich mit den Grundlagen der
objektorientierten Programmierung...', '2', '2021-12-01', '2021-12-15', 'ZA');

```

![Untitled](src/main/java/img/JDBC%20und%20D%20f1070/Untitled%201.png)

⇒ neues Maven-Projekt erstellen in InteliJ

⇒ Version 17

![Untitled](src/main/java/img/JDBC%20und%20D%20f1070/Untitled%202.png)

```java
<dependencies>
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>8.0.27</version>
        </dependency>
    </dependencies>
```

⇒ neue Klasse Main mit main-Methode

---

## Datenbankverbindung mit Singleton

Das Singelton-Pattern stellt zur Laufzeit fest, dass nur eine Instanz des Objekts gebildet wird. (Es wird nur dieses Objekt verwendet)

Bei einer Datenbankverbindung bietet sich dies an, da man nur einmalig eine Datenbankverbindung herstellen muss und diese kann man anschließend immer verwenden.

⇒ neues Package “dataaccess”

⇒ neue Klasse “MysqlDatabaseConnection” (unsere Singelton-Klasse)

⇒ wir machen ein statisches Feld vom Typ Connection und setzen diese auf null

⇒ erstellen eines Konstruktors, dieser wird auf private gesetzt, damit kein Objekt mit new gebildet werden kann.

### getConnection

⇒ neue Methode getConnection, benötigt immer drei Parameter (url, user, pwd) als Rückgabe erwarten wir eine Connection.

- zuerst wird in der IF-Bedingung überprüft ob eine Connection schon vorhanden ist, wenn diese vorhanden ist liefern wir diese zurück.
- Driver Name von der Klasse:
    - `Class.*forName*("com.mysql.cj.jdbc.Driver");`
    - Exeption ClassNotFoundException (wenn die Klasse noch nicht hier ist)
    - Ansonsten wir in die Variable con die Connection gespeichert die wir über die Klasse DriverManager mit der Methode getConnection bekommen. (möglich SQLExeption)

Mit dieser Klasse, können wir versuchen eine Datenbankverbindung aufzubauen.

URL:

`"jdbc:mysql://localhost:3306/kurssystem"`

---

## Kommandozeilenmenü

CLI = Comand Line Interface

Klasse die das Komandozeilenprogramm basies Implementiert

⇒ neues Package ui (user interface)

⇒ neue Klasse Cli erstellen

- wir benötigen einen Scanner in der Klasse Cli für die Eingaben (global)

---

## Domänenklassen

Bussines-Objekte für die Kursverwaltung

⇒ neues Package “domain”

⇒ Hauptklasse Course anlegen

Datenfelder:

```java
private String name;
    private String description;
    private int hours;
    private Date beginDate;
    private Date endDate;
    private CourseTyp courseTyp;
```

wir verwenden [java.sql.Date](http://java.sql.Date), da diese mit dem SQL-Format arbeitet.

⇒ neue ENUM-Klasse CourseTyp

```java
//Offene Einheit, begabten Förderung, Zusatz Angebot, Fachförderung
    OE, BF, ZA, FF
```

Alle Domainenklassen haben eines gemeinsam, die ID.

Somit Entitys (benötigen eine eindeutige Identifikationsnummer)

Deshalb würde sich es anbieten die ID in eine Abstrakte-Oberklasse auszulagern.

⇒ neue Abstrakte-Basisklasse “BaseEntity”

⇒ ein Datenfeld id vom Typ Long

Setter für ID: hat den Grund zum überprüfen, ob der Wert größer gleich 0 ist und ob der Wert noch null ist.

Über DatabaseAccessObjekt überprüfen wir ob der Wert noch null ist und wenn dieser Wert noch null ist, ist es für uns ein INSERT-Statement.

Die ID bekommen wir von der Datenbank geliefert.

```java
public void setId(Long id){
        if(id==null | id >= 0){
            this.id = id;
        }else {
            throw new InvalidValueException("Kurs-ID muss größer gleich 0 sein!");
        }
    }
```

mit throws InvalidValueException geben wir es dem Benutzer über die Methodensignatur weiter.

Die Methode before() kommt von JavaSQL

⇒ Getter und Setter generieren

Bei den Settern die jeweiligen überprüfungen durchführen und im Konstruktor geben wir die mitgelieferten Daten den jeweiligen Setter-Methoden mit.

2 Konstruktoren:

- Einmal mit ID mitgabe
- Einmal ohne ID mitgabe

---

## DAO Interfaces

Man möchte den Zugriff auf eine Datenbank abstrahieren, über eine vereinheitlichten Schnittstelle, die mit Datentransferobjekten arbeitet.

- Das DAO-Interface wird von einer implementierenden Klasse aus implementiert.
- Das DAO-Interface arbeitet mit den entsprechenden Entitys und wir haben hier unsere CLI-Klasse und diese verwendet das DAO-Interface um den Datenbankzugriff zu realisieren.
- Die Klasse CLI greift nicht direkt auf die konkrete MySQLDAO zugreift (kennt nur das Interface DAO / bekommt von außen diese Iniziirt con unserer Main)
- Grundidee:

![Untitled](src/main/java/img/JDBC%20und%20D%20f1070/Untitled%203.png)

⇒ neues Interface BaseRepository

Grundidee:

- gibt Standart-Crudoperationen vor
- dies Crudoperationen lassen wir von einem weiteren Repository erben und können anschließend speziellere Operationen ausführen.

Wir möchten zwei Dinge Parameterisieren:

Sollte nach jeder beliebiger Art von Entity funktionieren

```java
public interface BaseRepository<T,I>
```

- wir arbeiten hier mit generischen Typen, dass bedeutet später werden dann alle implementierenden Klassen von dem BaseRepository oder andere Interfaces die von dem BaseRepository erben, müssen eine zwei Typ Information mitgeben. (Platzhalter)
- wir möchten dass alle unsere Repositorys die wir implementieren eine insert Methode haben und wir möchten eine entity vom Typ “t” inserten.

    ```java
    Optional insert(T entity)
    ```

    - T bedeutet, dass wir mit allen beliebigen Referenztypen inserten möchten bzw. können.
    - Zurückgeben tun wir ein Typ Optional (ist ein Java-Typ), dies ist eine Wrapper-Klasse die ein Packet zurückliefert in dem etwas enthalten ist oder nicht. Wenn das INSERT-STATEMENT erfolgreich war, geben wir das eingefügte Element wieder zurück nach ausen, sonst Obtional ohne Inhalt.

  Alle unsere Repositorys oder DAO-Interfaces müssen INSERTEN können.

  T = in unserem Falle zB. Course Typ

  GetById:

  wir möchten eine Entität erhalten über die ID.

    ```java
    Optional getById(I id);
    ```

    - Daher benötigen wir den generischen Typ “I” zB. Long

  in unserem Fall wird hier die ID vom Typ Long mitgegeben. (kann auch ein anderer Typ sein)

  ⇒ neues Interface MyCourseRepository (Eigentliches DAO)

  Interfaces können von anderen Interfaces erben!


Hier geben wir die fixen Typen dem MutterInterface mit

- Course
- Long

```java
public interface MyCourseRepository extends BaseRepository
<Course, Long>
```

Wir hätten auch können die Typen Course und Long im BaseRepository angeben, so können wir aber für alle weiteren Repository das vorgefertigte Interface BaseRepository verwenden und erben lassen, mit den vorgefertigten Methodenköpfe.

- Wir können zusätzlich in dem Unterinterfaces zusätzliche Operationen hinzufügen.

MyCourseRepository = konkretes DAO

Was kann somit das Interface MyCourseRepository

- Standard Cruddoperationen
- spezifische Course Operationen, die wir vorgeben

---

## DAO Implementierung Teil 1 - CRUD

⇒ neue Klasse “MySqlCourseRepository”

ist nun gebrendet auf die jeweilige Technologie, die wir verwenden.

⇒ diese Klasse implementiert das Interface MyCourseRepository

wir haben zwei Interfaces, die voneinander erben, somit müssen wir alle Methoden der beiden Interfaces implementieren.

InteliJ checkt dies:

![Untitled](src/main/java/img/JDBC%20und%20D%20f1070/Untitled%204.png)

Die Platzhalter werden automatisch mit den Typen die wir benötigen ersetzt (Course, Long)

- Ab jetzt können wir mit konkreten Entitytypen arbeiten.

⇒ privates Datenfeld vom Typ Connection

⇒ wir erzeugen ein Parameter losen Konstruktor und setzen die Verbindung im Konstruktor einmalig.

⇒ Da wir in der CLI-Klasse von den Exception etwas wissen möchten(Benutzer) geben wir sie mit throw die jeweiligen Exceptions mit.

Konstruktor:

```java
public MySqlCourseRepository() throws SQLException, ClassNotFoundException {
            this.con = MysqlDatabaseConnection.getConnection(
"jdbc:mysql://localhost:3306/kurssystem", "root", "");

    }
```

- definieren die Datenbankverbindung (hier gehen wir in die Datenbank hinaus)
- hier definiert sich auch die Technologie (JDBC)

[resultSet.next](http://resultSet.next) liefert uns von dem Ergebnis des executeQuery den nächsten Datensatz

Objekt relationales Mapping

Wir haben eine Referenz auf MyCourseRepository, dass bedeutet unsere CLI benützt jetzt dieses Interface, sie weiss nichts von der konkreten Course implementierung.

Somit muss die Main-Methode dies iniziiren (simulieren).

⇒ Wir geben bei dem erzeugen der CLI dem Konstruktor ein konkretes CourseRepository mit. (MySqlCourseRepository mit der Implementierung). Somit entkoppelt

Wenn die Main dies nicht generieren kann fangen wir gleich die Exception und geben einen Fehler aus.

### getAll()

⇒ zuerst den SQL-Befehl

⇒ Erzeugen einer Variable vom Typ preparedStatment, dieser weisen wir den Methodenaufruf an der Verbindung con den prepareStatement auf und geben den SQL-Befehl mit.

⇒ erzeugen einer Variable vom Typ ResultSet, dieser weisen wir das Ergebnis, dass wir bekommen von dem Methodenaufruf executeQuery an der Variable prparedStatement zu.

⇒ erzeugen einer ArrayList die Objekte von Course halten kann.

⇒ wir iterieren über das ResultSet drüber mit der Methode next()

⇒ in unsere ArrayList können wir nur Objekte von Course halten, somit erzeugen wir immer ein neues Objekt von Course und weisen dieser die jeweiligen Daten aus der Datenbank zu.

⇒ CourseType ist in der Datenbank als String hinterlegt, in unserem Programm benötigen wir jedoch ein ENUM Typ von der Klasse CourseTyp. Somit können wir an der Klasse CourseTyp die Methode valueOf aufrufen und einen String mitliefern. (wir generieren uns ein ENUM von CourseTyp)

⇒ die ArrayList wird zurückgegeben

⇒ die CLI bekommt das CourseRepository hinein iniziiert.

---

## DAO Implementierung Teil 2 - CRUD

⇒ die Main Klasse startet die UI (Cli)-Klasse

⇒ bei der UI wird das CourseRepository mitgegeben

Die main-Methode ist zuständig für die Injektion der konkreten DAO implementierung.

⇒ Die CLI nimmt das MyCourseRepository entgegen und speichert dies in ein Datenfeld

⇒ Realisiert mit dem Repository den Datenbankzugriff

Objekt orientiertes Mapping:

Wir Mappen die Zeilen aus der Datenbank in konkrete Objekten oder in Listen von Course-Objekten

- Wir arbeiten an dieser Stelle nur noch mit Course-Objekte
- Wir haben an dieser Stelle keinen Betzug zu einer Datenbank-Techonolgie mehr (dies ist der Sinn des DAO-Patterns)
    - Zugriff wird Objket relational Gemappt
    - Der Programmierer der für die UI zuständig ist, weiss von der Datenbank-Technologie nichts mehr
    - Er benötigt auch keine SQL-Statements, er benötigt nur ein CourseRepository, eine implementierung des Interfaces (Applikationsprogrammierer (Datenbankzugriff) muss sich darum kümmern)
    - Der Rest arbeitet nur noch mit Course-Objekten


Ziel von DAO ist nicht nur das der Datenbankzugriff weg kommt, sondern auch das mapping generiert wird.

CLI kann mit den Coursen so arbeiten, als wäre es eine Liste im Speicher.

bei Alle Kurse anzeigen haben wir einen Klammerungsfehler gehabt, daher ist nur ein Datensatz ausgegeben worden.

---

## JDBC und DAO getById

⇒ neues Package “util”

⇒ neue Klasse “Accert”

Diese Klasse macht nichts anderes, als unsere Null checks durchzuführen.

⇒ eine statische Methode notNull

```java
if(o == null) throw new IllegalArgumentException("Reference must not be null!");
```

Da wir wissen möchten ob ein Datensatz mit der ID existiert erzeugen wir eine Hilfsmethode!

```java
private int countCoursesInDbWithId(Long id){
        try {
            String countSql = "SELECT COUNT(*) FROM `courses` WHERE `id`=?";
            PreparedStatement preparedStatement = con.prepareStatement(countSql);
            preparedStatement.setLong(1, id);
            ResultSet resultSetCount = preparedStatement.executeQuery();
            resultSetCount.next();
            int courseCount = resultSetCount.getInt(1);
            return courseCount;
        }catch (SQLException sqlException){
            throw new DatabaseException(sqlException.getMessage());
        }
    }
```

Methode getById, die uns ein Optional zurückliefert

```java
@Override
    public Optional<Course> getById(Long id) {
				//überprüfung auf null
        Assert.notNull(id);
				//Wenn Anzahl dDatensätze in der Datenbank sind gleich 0
        if(countCoursesInDbWithId(id) == 0){
						//gib ein leeres Optional zurück
            return Optional.empty();
        }else{
            try {
								//SQL-Statement
                String sql = "SELECT * FROM `courses` WHERE `id` = ?";
								//An der Verbindung wird der SQL-Befehl mitgegeben
                PreparedStatement preparedStatement = con.prepareStatement(sql);
								//Was wird bei dem ? gesetzt 
                preparedStatement.setLong(1, id);
								//Das Ergebnis des executeQuery wird in einem ResultSet gespespeichert
                ResultSet resultSet = preparedStatement.executeQuery();
								//Der nächste Datensatz 
                resultSet.next();
								//neuses Objekt von Course
                Course course = new Course(
                        resultSet.getLong(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getInt(4),
                        resultSet.getDate(5),
                        resultSet.getDate(6),
                        CourseTyp.valueOf(resultSet.getString(7))
                );
                return Optional.of(course);
            }catch (SQLException sqlException){
                throw new DatabaseException(sqlException.getMessage());
            }
        }
    }
```

```java
private void showCourseDetails() {
        System.out.println("Für welchen Kurs möchten Sie die Kursdetails anzeigen?");
				//CourseId wird vom Benutzer eingegeben und diese wird geparst und gespeichert
        Long courseId = Long.parseLong(scanner.nextLine());
        try {
						//an dem Repo wird die Methode aufgerufen
            Optional<Course> courseOptional = repo.getById(courseId);
						//wenn ein courseOptional vorhanden ist
            if(courseOptional.isPresent()){
						//wird die get-Methode aufgerufen und ausgegeben
                System.out.println(courseOptional.get());
            }else {
                System.out.println("Kurs mit der ID "+ courseId + " nicht gefunden!");
            }

        }catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler bei Kurs-Detailanzeigen: " + databaseException.getMessage());
        }catch (Exception exception){
            System.out.println("Unbekannter Fehler bei Kurs-Detailsanzeigen: " + exception.getMessage());
        }
    }
```

---

## CRUD - CREATE

Die insert Methode gibt ein Optional<Course> zurück. Wir bekommen ein Course entity als Parameter ohne ID mit und returnd wird ein Course entity mit ID.

`Statement.*RETURN_GENERATED_KEYS` = wir möchten den generierten Schlüssel separat zurück bekommen.*

Objekt relationales Mapping:

Wir bekommen eine Course-Objekt und holen uns von dem Courseobjekt einen Teil heraus und belegen, damit unser SQL-Statement. (in Richtung Datenbank, andere Richtung)

`int affectedRows = preparedStatement.executeUpdate();`

Die Anzahl der betroffenen Spalten betroffen sind.

`ResultSet resultSet = preparedStatement.getGeneratedKeys();`

Was wir uns aus dem resultSet zurückholen, sind die GeneratedKeys

Wir rufen an dem GeneratedKeys die Methode next auf (in unserem Fall ist es nur ein INSERT)

`this.getById(generatedKeys.getLong(1)`

Wir holen uns mit getLong die ID heraus und speichern sie sozusagen in eine Tabelle mit nur einer Spalte. mit getById holen wir uns das neue Objekt aus der Datenbank heraus und liefern dies als Optional vom Typ Course zurück.

⇒ Methode addCourse

- definieren von temporären Variablen

```java
if(name.equals("")) throw new IllegalArgumentException("Eingabe darf nicht leer sein!"));
```

an dieser Stelle machen wir UI-Validierung. Man könnte diese Eingabe-Validierung auch in der Klasse Course machen.

Validierung:

- Eingabe-Validierung
- Direkt beim Business-Objekt sicherstellen, dass es nie in einem inkonsistenten Zustand landen kann.

Ich habe einen Fehler in der Course Klasse gemacht. Habe bei dem Validieren bei setId ein | gemacht statt ||

---

## CRUD UPDATE

`this.getById(entity.getId()`

Wir holen den frischen Datensatz heraus.

```java
Optional<Course> courseOptionalUpdated = repo.update(
                        new Course(
                                course.getId(),
                                name.equals("") ? course.getName() : name,
                                description.equals("") ? course.getDescription() : description,
                                hours.equals("") ? course.getHours() : Integer.parseInt(hours),
                                dateFrom.equals("") ? course.getBeginDate() : Date.valueOf(dateFrom),
                                dateTo.equals("") ? course.getEndDate() : Date.valueOf(dateTo),
                                courseType.equals("") ? course.getCourseTyp() : CourseTyp.valueOf(courseType)
                        )
```

⇒ neue Variable vom Typ Optional<Course>

⇒ an dem repo rufen wir die update Funktion auf

⇒ wir geben der update Funktion ein neues Objekt des Course mit

⇒ wenn zB. der Name gleich leer ist holen wir an dem bestehenden Course-Objekt mit getName den namen, ansonsten wird der neue mitgegebene Name gesetzt.

```java
courseOptionalUpdated.ifPresentOrElse(
                        (c)-> System.out.println("Kurs aktualisiert: " + c),
                        ()-> System.out.println("Kurs konnte nicht aktualisiert werden!")
                );
```

Bedeutet nichts anderes:

Wir geben hier 2 Funktionen mit

- 1 Funktion übernimmt den Course der Upgedatet wurde, da dieser present ist
    - Wenn er present ist bekommen wir in (c) wie ein Methoden-Parameter den neuen Course und machen ein System.out.println mit diesem Course
- 2 Funktion wird nur das zweite System.out.println ausgeführt

---

## CRUD DELETE

⇒ umändern des Rückgabetyps bei der Methode deleteById

⇒ wenn erfolgreich wird true zurückgegeben sonst false

**CLI**:

⇒ im try-catch Block überprüfe ich ob ein Long typ eingegeben wurde, sonst wird eine NumberFormatException geschmissen

⇒ anschließend überprüfe ich ob der wert der Methode deleteById auch true zurück liefert wenn ja wird das erste System.out.println ausgeführt sonst der zweite.

---

## Kurssuche

Was die Datenbank gut kann, sollte man auch die Datenbank machen lassen.

zB. das Suchen in der Datenbank

---

## Aktuell laufende Kurse

Wir holen uns alle Spalten aus der Courses-Tabelle, wo NOW() liefert uns das aktuelle Datum und das wird vergliechen, ob es kleiner ist als das enddate

---

# JDBC und DAO - STUDENTEN

### Aufgabenstellung

Erweitere die fertig nachprogrammierte Applikation mit einem DAO für CRUD für eine neue Domänenklasse
„Student“:
• Studenten haben einen eine Student-ID, einen VN, einen NN, ein Geburtsdatum
• Domänenklasse implementieren (Setter absichern, neue Exceptions definieren, Business-Regeln selbst
wählen - z.B. dass Name nicht leer sein darf)
• Eigenes Interface MyStudentRepository von BaseRepository ableiten. MyStudentRepository muss
mindestens 3 studentenspezifische Methoden enthalten (z.B. Studentensuche nach Namen, Suche
nach ID, Suche nach bestimmtem Geburtsjahr, Suche mit Geburtsdatum zwischen x und y etc.).
• Implementierung von MyStudentRepository durch eine neue Klasse MySqlStudentRepository analog
zum MySqlCourseRepository.
• Erweiterung des CLI für die Verarbeitung von Studenten und für spezifische Studenten-Funktionen
(z.B. Student nach dem Namen suchen)

## Erstellen der Tabelle Student

![Untitled](src/main/java/img/JDBC%20und%20D%20f1070/Untitled%205.png)

⇒ Datensatz einfügen

- firstname = Clemens
- lastname = Kerber
- bithdate = 1999-06-22

⇒ Datensatz wurde erfolgreich erstellt:

![Untitled](src/main/java/img/JDBC%20und%20D%20f1070/Untitled%206.png)

---

## Erstellen der Klasse Student

drei Datenfelder:

- firstname
- lastname
- birthdate

zwei Konstruktoren

- einen mit ID mitgabe
- einen ohne ID mitgabe

⇒ toString, Getter und Setter generieren und anpassen

setFirstname und setLastname mit der Standartüberprüfung ob nicht leer oder nur mehr als ein Zeichen vorhanden sind.

setBirthdate():

```java
public void setBirthdate(Date birthdate) {
        java.util.Date currentTime = new java.util.Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        if(birthdate != null){
            if (birthdate.before(currentTime)){
                this.birthdate = birthdate;
            }else{
                throw new InvalidValueException("Student Geburtstag darf nicht in der Zukunft liegen!");
            }
        }else{
            throw new InvalidValueException("Student Geburtstag darf nicht leer sein!");
        }
    }
```

⇒ heutiges Datum holen wir uns an der Klasse Date von java.util

⇒ Formatt anpassen

⇒ Überprüfen ob das birthdate nicht null ist

⇒ Wenn das Geburtsdatum vor dem heutigen Datum ist, ist es gültig und das Datum wird gesetzt.

---

## Erstellen des Interfaces MyStudentRepository

⇒ Das Interface MyStudenmtRepostitroy erbt vom BaseRepository, wir geben dem BaseRepository den Objekt Typ Student und den Primarykey vom Typ Long mit.

⇒ fünf Methodenköpfe wurden definiert im Interface MyStudent Repository

---

## Erstellen der Klasse MySqlStudentRepository

⇒ implementiert das interface MyStudentRepositroy

⇒ ein Datenfeld vom Typ Connection

⇒ bei der Instanz erzeugung wird die Connection gesetzt

⇒ Aus implementieren von den Standart-CRUD-Methoen, das konzept ist das selbe wie bei der MySqlCourseRepository.

`public List<Student> findStudentBetweenById(Long idstart, Long idend)`

```sql
SELECT * FROM `student` WHERE `id` BETWEEN ? AND ?
```

Mitgabe von 2 ID Parameter. Die Datenbank sucht anschließend zwischen den beiden IDs und fügt diese in die ArrayList hinzu.

`public List<Student> findStudentByBirthdateBetween(Date startDate, Date endDate) {`

```sql
SELECT * FROM `student` WHERE `birthdate` BETWEEN ? AND ?
```

Mitgabe von 2 Datum-Parameter. Die Datenbank sucht anschließend zwischen den beiden IDs und fügt dies in die ArrayList hinzu.


# JDBC und DAO - Buchungen

1. Erstellen einer Tabelle bookings

Hinzufügen der Fremdschlüssel in der Tabelle bookings

![Untitled](src/main/java/img/JDBC%20und%20D%20f1070/Untitled%207.png)

Tabelle bookings:

- id (int)
- courses_id (int)
- student_id (int)

### Klasse Booking

⇒ neues Package im Package domain “booking”

⇒ neue Klasse erstellen Booking

⇒ die Klasse Booking erweitert die Klasse BaseEntity (für die ID)

⇒ 3 Datenfelder:

Long course;

Long student;

Date date;

⇒ zwei Konstruktor erstellen, einmal mit ID mitgabe und einmal ohne

⇒ Getter und Setter erstellen und bei den Settern die Validierung überprüfen und die Setter im Konstruktor anwenden.

⇒ toString Methode generieren

### Interface MyBookingRepository

⇒ neues Package in dataaccess erstellen “booking”

⇒ neues Interface MyBookingRepository erstellen

⇒ diese Interface erweitert das Interface BaseRepositroy<> wir geben dort Booking als Typ mit und als ID Typ Long

⇒ definieren der Zusatz Operationen der Datenbank

z.B.

```java
public interface MyBookingRepository extends BaseRepository<Booking, Long> {
       List<Booking> findAllBookingsByCourseId(Long courseId);
    List<Booking> findAllBookingsByStudenteId(Long studentId);
    List<Booking> findAllBookingsByDate (Date date);
    List<Booking> findAllStudentsInCourseWithInformation(Long courseId);
    List<Booking> findAllCoursesByStudentWithInformation (Long studentId);
```

### MySqlBookingRepository

⇒ erzeugen einer neuen Klasse “MySqlBookingRepository” im Package dataacess/booking

⇒ die Klasse implementiert das Interface “MyBookingRepository”

⇒ implementieren aller Methoden von BaseRepository und MyBookingRepository

### SQL-Befehle für die Operationen

insert:

```sql
INSERT INTO `bookings` (`course_id`, `student_id`, `date`) VALUES (?, ?, ?)
```

getById:

```sql
SELECT * FROM `bookings` WHERE `id` = ?
```

getAll:

```sql
SELECT * FROM `bookings`
```

update:

```sql
UPDATE `bookings` SET `course_id` = ?, `student_id` = ?, `date` = ? WHERE `bookings`.`id` = ?
```

deleteById:

```sql
DELETE FROM `bookings` WHERE `id` = ?
```

findAllBookingsByCourseId:

```sql
SELECT * FROM `bookings` WHERE course_id = ?
```

findAllBookingsByStudentId

```sql
SELECT * FROM `bookings` WHERE student_id = ?
```

findAllBookingsByDate

```sql
SELECT * FROM `bookings` WHERE date = ?
```

findAllStudentsInCourseWithInformation

```sql
SELECT * FROM `bookings` WHERE course_id = ?
```

findAllCoursesByStudentWithInformation

```sql
SELECT * FROM `bookings` WHERE student_id = ?
```

### CLI Änderung

⇒ Neues Datenfeld vom Typ MyBookingRepository

⇒ Inizialisiert wird dieses Datenfeld durch den Konstruktor

⇒ Für jede Operation der Funktionen in MySqlBookingRepository benötigen wir wieder die bestimmten Funktionen zum ausgeben der Daten in der Kommandozeile

⇒ Zusätzlich die Methode showBookingMenue(), für eine schöne Menüauflistung und eine weitere Switch-Case-Anweisung