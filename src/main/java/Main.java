import dataaccess.booking.MySqlBookingRepository;
import dataaccess.course.MySqlCourseRepository;
import dataaccess.student.MySqlStudentRepository;
import ui.Cli;

import java.sql.SQLException;

public class Main {

    public static void main(String[] args) {

        Cli cli = null;

        try {
            cli = new Cli(new MySqlCourseRepository(), new MySqlStudentRepository(), new MySqlBookingRepository());
        } catch (SQLException e) {
            System.out.println("Datenbankfehler: " + e.getMessage() + " SQL State: " + e.getSQLState());
        } catch (ClassNotFoundException e) {
            System.out.println("Datenbankfehler: " + e.getMessage());
        }
        cli.start();
    }
}
