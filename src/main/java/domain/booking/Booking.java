package domain.booking;

import domain.BaseEntity;
import domain.course.Course;
import domain.student.Student;

import java.sql.Date;

public class Booking extends BaseEntity {
    private Long course;
    private Long student;
    private Date date;

    public Booking(Long id, Long course, Long student, Date date) {
        super(id);
        this.setCourse(course);
        this.setStudent(student);
        this.setDate(date);
    }

    public Booking(Long course, Long student, Date date) {
        super(null);
        this.setCourse(course);
        this.setStudent(student);
        this.setDate(date);
    }

    public Long getCourse() {
        return course;
    }

    public void setCourse(Long course) {
        this.course = course;
    }

    public Long getStudent() {
        return student;
    }

    public void setStudent(Long student) {
        this.student = student;
    }


    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Booking{" +
                "id=" + id +
                ", course=" + course +
                ", student=" + student +
                ", date=" + date +
                '}';
    }
}
