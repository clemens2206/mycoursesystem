package domain.course;

public enum CourseTyp {
    //Offene Einheit, begabten Förderung, Zusatz Angebot, Fachförderung
    OE, BF, ZA, FF
}
