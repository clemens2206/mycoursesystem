package domain.student;

import domain.BaseEntity;
import domain.InvalidValueException;

import java.sql.Date;
import java.text.SimpleDateFormat;

public class Student extends BaseEntity {

    private String firstname;
    private String lastname;
    private Date birthdate;

    public Student(Long id, String firstname, String lastname, Date birthdate) {
        super(id);
        this.setFirstname(firstname);
        this.setLastname(lastname);
        this.setBirthdate(birthdate);
    }

    public Student(String firstname, String lastname, Date birthdate){
        super(null);
        this.setFirstname(firstname);
        this.setLastname(lastname);
        this.setBirthdate(birthdate);
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {

        if(firstname != null && firstname.length() > 1) {
            this.firstname = firstname;
        }else {
            throw new InvalidValueException("Studenten Vorname darf nicht leer sein und muss mindestens 2 Zeichen enthalten!");
        }
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        if(lastname != null && firstname.length() > 1){
            this.lastname = lastname;
        }else{
            throw new InvalidValueException("Studenten Nachname darf nicht leer sein und muss mindesten 2 Zeichen enthalten!");
        }
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        java.util.Date currentTime = new java.util.Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        if(birthdate != null){
            if (birthdate.before(currentTime)){
                this.birthdate = birthdate;
            }else{
                throw new InvalidValueException("Student Geburtstag darf nicht in der Zukunft liegen!");
            }
        }else{
            throw new InvalidValueException("Student Geburtstag darf nicht leer sein!");
        }
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", birthdate=" + birthdate +
                '}';
    }
}
