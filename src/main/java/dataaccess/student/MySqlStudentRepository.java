package dataaccess.student;

import dataaccess.DatabaseException;
import dataaccess.MysqlDatabaseConnection;
import domain.student.Student;
import util.Assert;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MySqlStudentRepository implements MyStudentRepository{
    private Connection con;

    public MySqlStudentRepository() throws SQLException, ClassNotFoundException {
        this.con = MysqlDatabaseConnection.getConnection("jdbc:mysql://localhost:3306/kurssystem", "root", "");
    }

    @Override
    public Optional<Student> insert(Student entity) {
        Assert.notNull(entity);
        try {
            String sql = "INSERT INTO `student` (`firstname`, `lastname`, `birthdate`) VALUES (?,?,?)";
            PreparedStatement preparedStatement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1,entity.getFirstname());
            preparedStatement.setString(2,entity.getLastname());
            preparedStatement.setDate(3,entity.getBirthdate());

            int affectedRows = preparedStatement.executeUpdate();

            if(affectedRows == 0){
                return Optional.empty();
            }
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()){
                return this.getById(generatedKeys.getLong(1));
            }else{
                return Optional.empty();
            }
        }catch (SQLException sqlException){
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    @Override
    public Optional<Student> getById(Long id) {
        Assert.notNull(id);
        if (countStudentsInDbWithId(id) == 0){
            return Optional.empty();
        }else {
            try {
                String sql = "SELECT * FROM `student` WHERE `id` = ?";
                PreparedStatement preparedStatement = con.prepareStatement(sql);
                preparedStatement.setLong(1,id);
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.next();
                Student student = new Student(
                        resultSet.getLong(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getDate(4)
                );
                return Optional.of(student);
            } catch (SQLException sqlException) {
                throw new DatabaseException(sqlException.getMessage());
            }
        }
    }

    private int countStudentsInDbWithId(Long id) {
        try {
            String countSql = "SELECT COUNT(*) FROM `student` WHERE `id` = ?";
            PreparedStatement preparedStatement = con.prepareStatement(countSql);
            preparedStatement.setLong(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            int studentCount = resultSet.getInt(1);
            return studentCount;
        }catch (SQLException sqlException){
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    @Override
    public List<Student> getAll() {
        try {
            String sql = "SELECT * FROM `student`";
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Student> studentArrayList = new ArrayList<>();
            while (resultSet.next()){
                studentArrayList.add(new Student(
                        resultSet.getLong(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getDate(4)
                ));
            }
            return studentArrayList;

        }catch (SQLException sqlException){
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    @Override
    public Optional<Student> update(Student entity) {
        Assert.notNull(entity);
        if (countStudentsInDbWithId(entity.getId()) == 0) {
            return Optional.empty();
        } else {
            try {
                String sql = "UPDATE `student` SET `firstname` = ?, `lastname` = ?, `birthdate` = ? WHERE `student`.`id` = ?";
                PreparedStatement preparedStatement = con.prepareStatement(sql);
                preparedStatement.setString(1, entity.getFirstname());
                preparedStatement.setString(2, entity.getLastname());
                preparedStatement.setDate(3, entity.getBirthdate());
                preparedStatement.setLong(4, entity.getId());

                int affectedRows = preparedStatement.executeUpdate();
                if(affectedRows == 0){
                    return Optional.empty();
                }else{
                    return this.getById(entity.id);
                }
            } catch (SQLException sqlException) {
                throw new DatabaseException(sqlException.getMessage());
            }
        }
    }

    @Override
    public boolean deleteById(Long id) {
        Assert.notNull(id);
        try {
            String sql = "DELETE FROM `student` WHERE `student`.`id` = ?";
            if (countStudentsInDbWithId(id) == 1) {
                PreparedStatement preparedStatement = con.prepareStatement(sql);
                preparedStatement.setLong(1, id);
                preparedStatement.executeUpdate();
                return true;
            }else {
                return false;
            }
        }catch (SQLException sqlException){
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    @Override
    public List<Student> findAllStudentsByFirstname(String firstname) {
        Assert.notNull(firstname);
        try {
            String sql = "SELECT * FROM `student` WHERE `firstname` LIKE ?";
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1, "%" + firstname + "%");
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Student> studentArrayList = new ArrayList<>();
            while (resultSet.next()){
                studentArrayList.add(new Student(
                        resultSet.getLong(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getDate(4)
                ));
            }
            return studentArrayList;
        }catch (SQLException sqlException){
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    @Override
    public List<Student> findAllStudentsByLastname(String lastname) {
        Assert.notNull(lastname);
        try {
            String sql = "SELECT * FROM `student` WHERE `lastname` LIKE ?";
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1, "%" + lastname + "%");
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Student> studentArrayList = new ArrayList<>();
            while (resultSet.next()){
                studentArrayList.add(new Student(
                        resultSet.getLong(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getDate(4)
                ));
            }
            return studentArrayList;
        }catch (SQLException sqlException){
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    @Override
    public List<Student> findAllStudentsByBirthdate(Date birthdate) {
        Assert.notNull(birthdate);
        try {
            String sql = "SELECT * FROM `student` WHERE `birthdate` LIKE ?";
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setDate(1, birthdate);
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Student> studentArrayList = new ArrayList<>();
            while (resultSet.next()){
                studentArrayList.add(new Student(
                        resultSet.getLong(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getDate(4)
                ));
            }
            return studentArrayList;
        }catch (SQLException sqlException){
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    public List<Student> findStudentBetweenById(Long idstart, Long idend) {
        Assert.notNull(idstart);
        Assert.notNull(idend);
        try {
            String sql = "SELECT * FROM `student` WHERE `id` BETWEEN ? AND ?";
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setLong(1,idstart);
            preparedStatement.setLong(2, idend);
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Student> studentArrayList = new ArrayList<>();
            while (resultSet.next()){
                studentArrayList.add(new Student(
                        resultSet.getLong(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getDate(4)
                ));
            }
            return studentArrayList;
        }catch (SQLException sqlException){
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    @Override
    public List<Student> findStudentByBirthdateBetween(Date startDate, Date endDate) {
        Assert.notNull(startDate);
        Assert.notNull(endDate);

        try {
            String sql = "SELECT * FROM `student` WHERE `birthdate` BETWEEN ? AND ?";
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setDate(1, startDate);
            preparedStatement.setDate(2, endDate);
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Student> studentArrayList = new ArrayList<>();
            while (resultSet.next()){
                studentArrayList.add(new Student(
                        resultSet.getLong(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getDate(4)
                ));
            }
            return studentArrayList;
        }catch (SQLException sqlException){
            throw new DatabaseException(sqlException.getMessage());
        }

    }
}
