package dataaccess.student;

import dataaccess.BaseRepository;
import domain.student.Student;

import java.sql.Date;
import java.util.List;

public interface MyStudentRepository extends BaseRepository<Student, Long> {
    List<Student> findAllStudentsByFirstname(String firstname);
    List<Student> findAllStudentsByLastname(String lastname);
    List<Student> findAllStudentsByBirthdate(Date birthdate);
    List<Student> findStudentBetweenById(Long idstart, Long idend);
    List<Student> findStudentByBirthdateBetween(Date startDate, Date endDate);
}
