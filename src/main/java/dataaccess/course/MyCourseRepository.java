package dataaccess.course;

import dataaccess.BaseRepository;
import domain.course.Course;
import domain.course.CourseTyp;

import java.sql.Date;
import java.util.List;

public interface MyCourseRepository extends BaseRepository<Course, Long> {
        List<Course> findAllCoursesByName(String name);
        List<Course> findAllCoursesByDescription(String description);
        List<Course> findAllCoursesByNameOrDescription(String searchText);
        List<Course> findAllCoursesByStartDate(Date startDate);
        List<Course> findAllCoursesByCourseType(CourseTyp courseTyp);
        List<Course> findAllRunningCourses();
}
