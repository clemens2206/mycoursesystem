package dataaccess.booking;

import dataaccess.BaseRepository;
import domain.booking.Booking;
import domain.course.Course;
import domain.student.Student;

import java.sql.Date;
import java.util.List;

public interface MyBookingRepositroy extends BaseRepository<Booking, Long> {
    List<Booking> findAllBookingsByCourseId(Long courseId);
    List<Booking> findAllBookingsByStudenteId(Long studentId);
    List<Booking> findAllBookingsByDate (Date date);
    List<Booking> findAllStudentsInCourseWithInformation(Long courseId);
    List<Booking> findAllCoursesByStudentWithInformation (Long studentId);
}
