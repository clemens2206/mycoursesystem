package dataaccess.booking;

import dataaccess.DatabaseException;
import dataaccess.MysqlDatabaseConnection;
import domain.booking.Booking;
import domain.course.Course;
import domain.course.CourseTyp;
import domain.student.Student;
import util.Assert;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MySqlBookingRepository implements MyBookingRepositroy {
    private Connection con;

    public MySqlBookingRepository() throws SQLException, ClassNotFoundException {
        this.con = MysqlDatabaseConnection.getConnection("jdbc:mysql://localhost:3306/kurssystem", "root", "");
    }

    @Override
    public Optional<Booking> insert(Booking entity) {
        Assert.notNull(entity);
        try {
            String sql = "INSERT INTO `bookings` (`course_id`, `student_id`, `date`) VALUES (?, ?, ?)";
            PreparedStatement preparedStatement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setLong(1, entity.getCourse());
            preparedStatement.setLong(2,entity.getStudent());
            preparedStatement.setDate(3,entity.getDate());

            int affectedRows = preparedStatement.executeUpdate();

            if(affectedRows == 0){
                return Optional.empty();
            }
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()){
                return this.getById(generatedKeys.getLong(1));
            }else{
                return Optional.empty();
            }
        }catch (SQLException sqlException){
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    @Override
    public Optional<Booking> getById(Long id) {
        Assert.notNull(id);
        if(countCoursesInDbWithId(id) == 0){
            return Optional.empty();
        }else{
            try {
                String sql = "SELECT * FROM `bookings` WHERE `id` = ?";
                PreparedStatement preparedStatement = con.prepareStatement(sql);
                preparedStatement.setLong(1, id);
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.next();
                Booking booking = new Booking(
                        resultSet.getLong(1),
                        resultSet.getLong(2),
                        resultSet.getLong(3),
                        resultSet.getDate(4)
                );
                return Optional.of(booking);
            }catch (SQLException sqlException){
                throw new DatabaseException(sqlException.getMessage());
            }
        }
    }

    private int countCoursesInDbWithId(Long id){
        try {
            String countSql = "SELECT COUNT(*) FROM `bookings` WHERE `id`=?";
            PreparedStatement preparedStatement = con.prepareStatement(countSql);
            preparedStatement.setLong(1, id);
            ResultSet resultSetCount = preparedStatement.executeQuery();
            resultSetCount.next();
            int courseCount = resultSetCount.getInt(1);
            return courseCount;
        }catch (SQLException sqlException){
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    @Override
    public List<Booking> getAll() {
        String sql = "SELECT * FROM `bookings`";
        try {
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Booking> bookingsList = new ArrayList<>();
            while (resultSet.next()){
                bookingsList.add(new Booking(
                                resultSet.getLong(1),
                                resultSet.getLong(2),
                                resultSet.getLong(3),
                                resultSet.getDate(4)
                        )
                );
            }
            return bookingsList;
        } catch (SQLException e) {
            throw new DatabaseException("Database error occured!");
        }
    }

    @Override
    public Optional<Booking> update(Booking entity) {

        Assert.notNull(entity);

        String sql = "UPDATE `bookings` SET `course_id` = ?, `student_id` = ?, `date` = ? WHERE `bookings`.`id` = ?";
        if(countCoursesInDbWithId(entity.getId())== 0){
            return Optional.empty();
        }else{
            try {
                PreparedStatement preparedStatement = con.prepareStatement(sql);
                preparedStatement.setLong(1,entity.getCourse());
                preparedStatement.setLong(2,entity.getStudent());
                preparedStatement.setDate(3,entity.getDate());
                preparedStatement.setLong(4,entity.getId());

                int affectedRows = preparedStatement.executeUpdate();
                if(affectedRows == 0){
                    return Optional.empty();
                }else{
                    return this.getById(entity.getId());
                }
            }catch (SQLException sqlException){
                throw new DatabaseException(sqlException.getMessage());
            }
        }
    }

    @Override
    public boolean deleteById(Long id) {
        Assert.notNull(id);

        String sql = "DELETE FROM `bookings` WHERE `id` = ?";
        try {
            if(countCoursesInDbWithId(id) == 1) {
                PreparedStatement preparedStatement = con.prepareStatement(sql);
                preparedStatement.setLong(1, id);
                preparedStatement.executeUpdate();
                return true;
            }else {
                return false;
            }
        }catch (SQLException sqlException){
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    @Override
    public List<Booking> findAllBookingsByCourseId(Long courseId) {
        Assert.notNull(courseId);

        try {
            String sql = "SELECT * FROM `bookings` WHERE course_id = ?";
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setLong(1, courseId);
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Booking> bookingArrayList = new ArrayList<>();
            while (resultSet.next()){
                bookingArrayList.add(new Booking(
                                resultSet.getLong(1),
                                resultSet.getLong(2),
                                resultSet.getLong(3),
                                resultSet.getDate(4))
                );
            }
            return bookingArrayList;
        }catch (SQLException sqlException){
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    @Override
    public List<Booking> findAllBookingsByStudenteId(Long studentId) {
        Assert.notNull(studentId);

        try {
            String sql = "SELECT * FROM `bookings` WHERE student_id = ?";
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setLong(1, studentId);
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Booking> bookingArrayList = new ArrayList<>();
            while (resultSet.next()){
                bookingArrayList.add(new Booking(
                        resultSet.getLong(1),
                        resultSet.getLong(2),
                        resultSet.getLong(3),
                        resultSet.getDate(4))
                );
            }
            return bookingArrayList;
        }catch (SQLException sqlException){
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    @Override
    public List<Booking> findAllBookingsByDate(Date date) {
        Assert.notNull(date);

        try {
            String sql = "SELECT * FROM `bookings` WHERE date = ?";
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setDate(1, date);
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Booking> bookingArrayList = new ArrayList<>();
            while (resultSet.next()){
                bookingArrayList.add(new Booking(
                        resultSet.getLong(1),
                        resultSet.getLong(2),
                        resultSet.getLong(3),
                        resultSet.getDate(4))
                );
            }
            return bookingArrayList;
        }catch (SQLException sqlException){
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    @Override
    public List<Booking> findAllStudentsInCourseWithInformation(Long courseId) {
        Assert.notNull(courseId);
        try {
            String sql = "SELECT * FROM `bookings` WHERE course_id = ?";
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setLong(1, courseId);
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Booking> bookingArrayList = new ArrayList<>();
            while (resultSet.next()) {
                bookingArrayList.add(new Booking(
                        resultSet.getLong(1),
                        resultSet.getLong(2),
                        resultSet.getLong(3),
                        resultSet.getDate(4))
                );
            }
            return bookingArrayList;
        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    @Override
    public List<Booking> findAllCoursesByStudentWithInformation(Long studentId) {
        Assert.notNull(studentId);
        try {
            String sql = "SELECT * FROM `bookings` WHERE student_id = ?";
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setLong(1, studentId);
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Booking> bookingArrayList = new ArrayList<>();
            while (resultSet.next()) {
                bookingArrayList.add(new Booking(
                        resultSet.getLong(1),
                        resultSet.getLong(2),
                        resultSet.getLong(3),
                        resultSet.getDate(4))
                );
            }
            return bookingArrayList;
        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }
    }
}
