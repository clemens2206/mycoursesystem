package ui;

import dataaccess.DatabaseException;
import dataaccess.booking.MyBookingRepositroy;
import dataaccess.course.MyCourseRepository;
import dataaccess.student.MyStudentRepository;
import domain.booking.Booking;
import domain.course.Course;
import domain.course.CourseTyp;
import domain.InvalidValueException;
import domain.student.Student;

import java.awt.print.Book;
import java.sql.Date;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class Cli {

    Scanner scanner;
    MyCourseRepository repo;
    MyStudentRepository studentRepository;
    MyBookingRepositroy bookingRepositroy;

    public Cli(MyCourseRepository myCourseRepository, MyStudentRepository myStudentRepository, MyBookingRepositroy myBookingRepositroy){

        this.scanner = new Scanner(System.in);
        this.repo = myCourseRepository;
        this.studentRepository = myStudentRepository;
        this.bookingRepositroy = myBookingRepositroy;
    }


    private void findAllCoursesByStudentWithInformation(){
        try {
            List<Booking> courseList;
            System.out.println("Geben Sie die Studenten-ID ein: ");
            Long studentid = Long.parseLong(scanner.nextLine());
            courseList = bookingRepositroy.findAllCoursesByStudentWithInformation(studentid);

            if(courseList.size() > 0) {
                for (Booking booking : courseList) {
                    System.out.println(repo.getById(booking.getCourse()));
                }
            }else{
                System.out.println("Kein Kurs für den Studenten mit der ID: " + studentid + " gefunden!");
            }

        }catch (IllegalArgumentException illegalArgumentException){
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
        }catch (InvalidValueException invalidValueException){
            System.out.println("Student nicht korrekt angegeben: " + invalidValueException.getMessage());
        }catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler beim Einfügen: " + databaseException.getMessage());
        }catch (Exception exception){
            System.out.println("Unbekannter Fehler beim Einfügen: " + exception.getMessage());
        }
    }

    private void findAllStudentsInCourseWithInformation(){
        try {
            List<Booking> studentList;
            System.out.println("Geben Sie die Kurs-ID ein: ");
            Long courseid = Long.parseLong(scanner.nextLine());
            studentList = bookingRepositroy.findAllStudentsInCourseWithInformation(courseid);

            if(studentList.size() > 0) {
                for (Booking booking : studentList) {
                    System.out.println(studentRepository.getById(booking.getStudent()));
                }
            }else{
                System.out.println("Kein Student mit der Kurs ID: " + courseid + " gefunden!");
            }

        }catch (IllegalArgumentException illegalArgumentException){
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
        }catch (InvalidValueException invalidValueException){
            System.out.println("Student nicht korrekt angegeben: " + invalidValueException.getMessage());
        }catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler beim Einfügen: " + databaseException.getMessage());
        }catch (Exception exception){
            System.out.println("Unbekannter Fehler beim Einfügen: " + exception.getMessage());
        }
    }

    private void findAllBookingsByDate() {
        System.out.println("Geben Sie das Buchungsdatum ein");
        String date = scanner.nextLine();
        List<Booking> bookingList;
        try {
            bookingList = bookingRepositroy.findAllBookingsByDate(Date.valueOf(date));
            for (Booking booking: bookingList) {
                System.out.println(booking);
            }
        }catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler bei der Suche per Datum: " + databaseException.getMessage());
        }catch (IllegalArgumentException illegalArgumentException){
            System.out.println("Unbekannte Eingabe: " + illegalArgumentException.getMessage());
        }catch (Exception exception){
            System.out.println("Unbekannter Fehler bei der Suche per Datum: " + exception.getMessage());
        }
    }

    private void findAllBookingsByStudentID() {
        System.out.println("Geben Sie die Student-ID ein");
        String studentId = scanner.nextLine();
        List<Booking> bookingList;
        try {
            bookingList = bookingRepositroy.findAllBookingsByStudenteId(Long.valueOf(studentId));
            for (Booking booking: bookingList) {
                System.out.println(booking);
            }
        }catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler bei der Suche per Student-ID: " + databaseException.getMessage());
        }catch (IllegalArgumentException illegalArgumentException){
            System.out.println("Unbekannte Eingabe: " + illegalArgumentException.getMessage());
        }catch (Exception exception){
            System.out.println("Unbekannter Fehler bei der Suche per Student-ID: " + exception.getMessage());
        }
    }

    private void findAllBookingsByCourseID() {
        System.out.println("Geben Sie die Kurs-ID ein");
        String courseId = scanner.nextLine();
        List<Booking> bookingList;
        try {
            bookingList = bookingRepositroy.findAllBookingsByCourseId(Long.valueOf(courseId));
            for (Booking booking: bookingList) {
                System.out.println(booking);
            }
        }catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler bei der Suche per Kurs-ID: " + databaseException.getMessage());
        }catch (IllegalArgumentException illegalArgumentException){
            System.out.println("Unbekannte Eingabe: " + illegalArgumentException.getMessage());
        }catch (Exception exception){
            System.out.println("Unbekannter Fehler bei der Suche per Kurs-ID: " + exception.getMessage());
        }
    }


    private void deleteBooking() {
        try {
            System.out.println("Welche Buchung möchten Sie entfernen (ID)");
            Long bookingId = Long.parseLong(scanner.nextLine());
            if (bookingRepositroy.deleteById(bookingId)){
                System.out.println("Buchung wurde erfolgreich gelöscht");
            }else{
                System.out.println("Buchung wurde nicht gelöscht");
            }
        }catch (NumberFormatException numberFormatException){
            System.out.println("Unbekanntes Format: " + numberFormatException);
        }catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler beim Löschen: " + databaseException.getMessage());
        } catch (Exception exception){
            System.out.println("Unbekannterfehler beim Löschen: " + exception.getMessage());
        }
    }

    private void getByIdbooking() {
        System.out.println("Für welche Buchung möchten Sie die Buchungsdetails anzeigen?");
        Long bookingId = Long.parseLong(scanner.nextLine());
        try {
            Optional<Booking> bookingOptional = bookingRepositroy.getById(bookingId);
            if(bookingOptional.isPresent()){
                System.out.println(bookingOptional.get());
            }else {
                System.out.println("Buchung mit der ID "+ bookingId + " nicht gefunden!");
            }

        }catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler bei Buchung-Detailanzeigen: " + databaseException.getMessage());
        }catch (Exception exception){
            System.out.println("Unbekannter Fehler bei Buchung-Detailsanzeigen: " + exception.getMessage());
        }
    }

    private void addBooking() {
        Long studentID;
        Long courseID;
        Date date;

        try {
            System.out.println("*******************STUDENTS************************");
            showAllStudents();
            System.out.println("********************COURSES***********************");
            showAllCourses();
            System.out.println("*******************************************");

            System.out.println("Bitte alle Buchungsdaten angeben:");
            System.out.println("Kurs ID: ");
            courseID = Long.parseLong(scanner.nextLine());
            System.out.println("Studenten ID: ");
            studentID = Long.parseLong(scanner.nextLine()) ;
            System.out.println("Buchungsdatum (YYYY-MM-DD): ");
            date = Date.valueOf(scanner.nextLine());

            Optional<Booking> optionalBooking = bookingRepositroy.insert(new Booking(
                    courseID, studentID, date));

            if (optionalBooking.isPresent()){
                System.out.println("Buchung angelegt: " + optionalBooking.get());
            }else{
                System.out.println("Buchung wurde nicht angelegt!");
            }


        }catch (IllegalArgumentException illegalArgumentException){
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
        }catch (InvalidValueException invalidValueException){
            System.out.println("Buchungsdaten nicht korrekt angegeben: " + invalidValueException.getMessage());
        }catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler beim Einfügen: " + databaseException.getMessage());
        }catch (Exception exception){
            System.out.println("Unbekannter Fehler beim Einfügen: " + exception.getMessage());
        }
    }

    private void showAllBookings() {
        List<Booking> bookingList;

        try {
            bookingList = bookingRepositroy.getAll();

            if (bookingList.size() > 0) {
                for (Booking booking : bookingList) {
                    System.out.println(booking);
                }
            } else {
                System.out.println("Buchungsliste ist leer!");
            }

        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler bei Buchungs-Detailanzeigen: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler bei Buchungs-Detailsanzeigen: " + exception.getMessage());
        }
    }

    private void updateBooking() {

        try {
            System.out.println("Für welche Buchungs-ID möchten Sie die Details ändern: ");
            Long id = Long.parseLong(scanner.nextLine());

            Optional<Booking> optionalBooking = bookingRepositroy.getById(id);
            if(optionalBooking == null){
                System.out.println("Buchungs-ID nicht in der Datenbank");
            }else {
                Booking booking = optionalBooking.get();

                System.out.println("Änderungen für folgenden Studenten");
                System.out.println(booking);

                String coursid, sutdentid, date;

                System.out.println("Bitte neue Buchungsdaten angeben (Enter, falls keine Änderung gewünscht ist): ");
                System.out.println("Kurs-ID: ");
                coursid = scanner.nextLine();
                System.out.println("Studenten-ID: ");
                sutdentid = scanner.nextLine();
                System.out.println("Buchungsdatum (YYYY-MM-DD): ");
                date = scanner.nextLine();

                Optional<Booking> bookingOptionalUpdated = bookingRepositroy.update(
                        new Booking(
                                booking.getId(),
                                coursid.equals("") ? booking.getCourse() : Long.parseLong(coursid),
                                sutdentid.equals("") ? booking.getStudent() : Long.parseLong(sutdentid),
                                date.equals("") ? booking.getDate() : Date.valueOf(date)
                        )
                );
                bookingOptionalUpdated.ifPresentOrElse(
                        (c)-> System.out.println("Buchung aktualisiert: " + c),
                        ()-> System.out.println("Buchung konnte nicht aktualisiert werden!")
                );
            }

        }catch (IllegalArgumentException illegalArgumentException){
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
        }catch (InvalidValueException invalidValueException){
            System.out.println("Buchung nicht korrekt angegeben: " + invalidValueException.getMessage());
        }catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler beim Einfügen: " + databaseException.getMessage());
        }catch (Exception exception){
            System.out.println("Unbekannter Fehler beim Einfügen: " + exception.getMessage());
        }
    }

    private void findStudentBetweenBithdate() {
        Date startdate, enddate;
        List<Student> studentList;
        try {
            System.out.println("Geben Sie das Startdatum ein (YYYY-MM-DD): ");
            startdate= Date.valueOf(scanner.nextLine());
            System.out.println("Geben Sie das Enddatum ein (YYYY-MM-DD): ");
            enddate= Date.valueOf(scanner.nextLine());
            studentList = studentRepository.findStudentByBirthdateBetween(startdate,enddate);
            if(studentList.size()> 0){
                for (Student student: studentList) {
                    System.out.println(student);
                }
            }else{
                System.out.println("Studentenliste ist leer!");
            }
        }catch (IllegalArgumentException illegalArgumentException) {
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
        } catch (InvalidValueException invalidValueException) {
            System.out.println("Student nicht korrekt angegeben: " + invalidValueException.getMessage());
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler beim Einfügen: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler beim Einfügen: " + exception.getMessage());
        }
    }

    private void findStudentBetweenById() {
        List<Student> studentList;
        try {
            System.out.println("Geben Sie die Start-ID der gesuchten Studenten ein: ");
            Long idstart = Long.valueOf(scanner.nextLine());
            System.out.println("Geben Sie die End-ID der gesuchten Studenten ein: ");
            Long idend = Long.valueOf(scanner.nextLine());
            studentList = studentRepository.findStudentBetweenById(idstart, idend);
            if(studentList.size() > 0 ){
                for (Student student: studentList) {
                    System.out.println(student);
                }
            }else{
                System.out.println("Die Studentenliste ist leer");
            }

        } catch (IllegalArgumentException illegalArgumentException) {
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
        } catch (InvalidValueException invalidValueException) {
            System.out.println("Student nicht korrekt angegeben: " + invalidValueException.getMessage());
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler beim Einfügen: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler beim Einfügen: " + exception.getMessage());
        }
    }

    private void findStudentByBirthdate() {
        List<Student> studentList;
        try {
            System.out.println("Geben Sie das gesuchte Geburtsdatum ein (YYYY-MM-DD): ");
            String birthdate = scanner.nextLine();
            studentList = studentRepository.findAllStudentsByBirthdate(Date.valueOf(birthdate));
            if(studentList.size() > 0){
                for (Student student: studentList) {
                    System.out.println(student);
                }
            }else{
                System.out.println("Keine Studenten vorhanden");
            }
        }catch (IllegalArgumentException illegalArgumentException){
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
        }catch (InvalidValueException invalidValueException){
            System.out.println("Student nicht korrekt angegeben: " + invalidValueException.getMessage());
        }catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler beim Einfügen: " + databaseException.getMessage());
        }catch (Exception exception){
            System.out.println("Unbekannter Fehler beim Einfügen: " + exception.getMessage());
        }
    }

    private void findStudentByLastname() {
        try {
            List<Student> studentList;
            System.out.println("Geben Sie den gesuchten Nachnamen ein: ");
            String searchname = scanner.nextLine();
            studentList = studentRepository.findAllStudentsByLastname(searchname);

            if(studentList.size() > 0) {
                for (Student student : studentList) {
                    System.out.println(student);
                }
            }else{
                System.out.println("Kein Student mit dem Vornamen " + searchname + " gefunden!");
            }

        }catch (IllegalArgumentException illegalArgumentException){
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
        }catch (InvalidValueException invalidValueException){
            System.out.println("Student nicht korrekt angegeben: " + invalidValueException.getMessage());
        }catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler beim Einfügen: " + databaseException.getMessage());
        }catch (Exception exception){
            System.out.println("Unbekannter Fehler beim Einfügen: " + exception.getMessage());
        }
    }

    private void findStudentByFirstname() {
        try {
            List<Student> studentList;
            System.out.println("Geben Sie den gesuchten Vornamen ein: ");
            String searchname = scanner.nextLine();
            studentList = studentRepository.findAllStudentsByFirstname(searchname);

            if(studentList.size() > 0) {
                for (Student student : studentList) {
                    System.out.println(student);
                }
            }else{
                System.out.println("Kein Student mit dem Vornamen " + searchname + " gefunden!");
                }

        }catch (IllegalArgumentException illegalArgumentException){
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
        }catch (InvalidValueException invalidValueException){
            System.out.println("Student nicht korrekt angegeben: " + invalidValueException.getMessage());
        }catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler beim Einfügen: " + databaseException.getMessage());
        }catch (Exception exception){
            System.out.println("Unbekannter Fehler beim Einfügen: " + exception.getMessage());
        }
    }

    private void deleteStudent() {
        try {
            System.out.println("Welchen Studenten möchten Sie entfernen (ID)");
            Long studentId = Long.parseLong(scanner.nextLine());
            if (studentRepository.deleteById(studentId)){
                System.out.println("Student wurde erfolgreich gelöscht");
            }else{
                System.out.println("Student wurde nicht gelöscht");
            }
        }catch (NumberFormatException numberFormatException){
            System.out.println("Unbekanntes Format: " + numberFormatException);
        }catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler beim Löschen: " + databaseException.getMessage());
        } catch (Exception exception){
            System.out.println("Unbekannterfehler beim Löschen: " + exception.getMessage());
        }
    }

    private void updateStudent() {

        try {
            System.out.println("Für welche Studenten-ID möchten Sie die Details ändern: ");
            Long id = Long.parseLong(scanner.nextLine());

            Optional<Student> optionalStudent = studentRepository.getById(id);
            if(optionalStudent == null){
                System.out.println("Studenten-ID nicht in der Datenbank");
            }else {
                Student student = optionalStudent.get();

                System.out.println("Änderungen für folgenden Studenten");
                System.out.println(student);

                String firstname, lastname, birthdate;

                System.out.println("Bitte neue Studentendaten angeben (Enter, falls keine Änderung gewünscht ist): ");
                System.out.println("Vorname: ");
                firstname = scanner.nextLine();
                System.out.println("Nachname: ");
                lastname = scanner.nextLine();
                System.out.println("Geburtstag (YYYY-MM-DD): ");
                birthdate = scanner.nextLine();

                Optional<Student> studentOptionalUpdated = studentRepository.update(
                        new Student(
                                student.getId(),
                                firstname.equals("") ? student.getFirstname() : firstname,
                                lastname.equals("") ? student.getLastname() : lastname,
                                birthdate.equals("") ? student.getBirthdate() : Date.valueOf(birthdate)
                        )
                );
                studentOptionalUpdated.ifPresentOrElse(
                        (c)-> System.out.println("Kurs aktualisiert: " + c),
                        ()-> System.out.println("Kurs konnte nicht aktualisiert werden!")
                );
            }

        }catch (IllegalArgumentException illegalArgumentException){
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
        }catch (InvalidValueException invalidValueException){
            System.out.println("Student nicht korrekt angegeben: " + invalidValueException.getMessage());
        }catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler beim Einfügen: " + databaseException.getMessage());
        }catch (Exception exception){
            System.out.println("Unbekannter Fehler beim Einfügen: " + exception.getMessage());
        }
    }

    private void showAllStudents() {
        List<Student> studentList;

        try {
            studentList = studentRepository.getAll();

            if(studentList.size() > 0){
                for (Student student: studentList) {
                    System.out.println(student);
                }
            }else{
                System.out.println("Studenten Liste ist leer!");
            }

        }catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler bei Student-Detailanzeigen: " + databaseException.getMessage());
        }catch (Exception exception){
            System.out.println("Unbekannter Fehler bei Student-Detailsanzeigen: " + exception.getMessage());
        }
    }

    private void showStudentDetails() {
        System.out.println("Für welchen Studenten möchten Sie die Studentendetails anzeigen?");
        Long studentId = Long.parseLong(scanner.nextLine());
        try {
            Optional<Student> studentOptional = studentRepository.getById(studentId);
            if(studentOptional.isPresent()){
                System.out.println(studentOptional.get());
            }else {
                System.out.println("Student mit der ID "+ studentId + " nicht gefunden!");
            }

        }catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler bei Student-Detailanzeigen: " + databaseException.getMessage());
        }catch (Exception exception){
            System.out.println("Unbekannter Fehler bei Student-Detailsanzeigen: " + exception.getMessage());
        }
    }

    private void addStudent() {
        String firstname, lastname;
        Date birthdate;

        try {
            System.out.println("Bitte alle Studentendaten angeben:");
            System.out.println("Vorname: ");
            firstname = scanner.nextLine();
            if (firstname.equals("")) throw new IllegalArgumentException("Eingabe darf nicht leer sein!");
            System.out.println("Nachname: ");
            lastname = scanner.nextLine();
            if (lastname.equals("")) throw new IllegalArgumentException("Eingabe darf nicht leer sein!");
            System.out.println("Geburtstag (YYYY-MM-DD): ");
            birthdate = Date.valueOf(scanner.nextLine());

            Optional<Student> optionalStudent = studentRepository.insert(new Student(
                    firstname, lastname, birthdate));

            if (optionalStudent.isPresent()) {
                System.out.println("Student angelegt: " + optionalStudent.get());
            } else {
                System.out.println("Student wurde nicht angelegt!");
            }
        }catch (IllegalArgumentException illegalArgumentException){
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
        }catch (InvalidValueException invalidValueException){
            System.out.println("Student nicht korrekt angegeben: " + invalidValueException.getMessage());
        }catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler beim Einfügen: " + databaseException.getMessage());
        }catch (Exception exception){
            System.out.println("Unbekannter Fehler beim Einfügen: " + exception.getMessage());
        }
    }

    private void findPerCourseTyp() {
        System.out.println("Geben Sie den Kurstyp ein für die Suche: ");
        String searchCT = scanner.nextLine();
        List<Course> courseList;
        try {
            courseList = repo.findAllCoursesByCourseType(CourseTyp.valueOf(searchCT));
            for (Course course: courseList) {
                System.out.println(course);
            }
        }catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler bei der Suche nach dem Kurstyp: " + databaseException.getMessage());
        }catch (IllegalArgumentException illegalArgumentException){
            System.out.println("Unbekannte Eingabe: " + illegalArgumentException.getMessage());
        }catch (Exception exception){
            System.out.println("Unbekannter Fehler bei der Suche nach dem Kurstyp: " + exception.getMessage());
        }
    }

    private void findPerstartDate() {
        System.out.println("Geben Sie ein Startdatum ein");
        String searchDate = scanner.nextLine();
        List<Course> courseList;
        try {
            courseList = repo.findAllCoursesByStartDate(Date.valueOf(searchDate));
            for (Course course: courseList) {
                System.out.println(course);
            }
        }catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler bei der Suche per Datum: " + databaseException.getMessage());
        }catch (IllegalArgumentException illegalArgumentException){
            System.out.println("Unbekannte Eingabe: " + illegalArgumentException.getMessage());
        }catch (Exception exception){
            System.out.println("Unbekannter Fehler bei der Suche per Datum: " + exception.getMessage());
        }
    }

    private void findPerDescription() {
        System.out.println("Geben Sie einen Suchbegriff ein: ");
        String search = scanner.nextLine();
        List<Course> courseList;
        try {
            courseList = repo.findAllCoursesByDescription(search);
            for (Course course: courseList) {
                System.out.println(course);
            }
        }catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler bei der Kurssuche: " + databaseException.getMessage());
        }catch (Exception exception){
            System.out.println("Unbekannter Fehler bei der Kurssuche: " + exception.getMessage());
        }
    }

    private void findPerName() {
        System.out.println("Geben Sie einen Suchbegriff ein: ");
        String search = scanner.nextLine();
        List<Course> courseList;
        try {
            courseList = repo.findAllCoursesByName(search);
            for (Course course: courseList) {
                System.out.println(course);
            }
        }catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler bei der Kurssuche: " + databaseException.getMessage());
        }catch (Exception exception){
            System.out.println("Unbekannter Fehler bei der Kurssuche: " + exception.getMessage());
        }
    }

    private void runningCourses() {
        System.out.println("Aktuell laufende Kurse: ");
        List<Course> courseList;
        try {
           courseList =  repo.findAllRunningCourses();
            for (Course course : courseList) {
                System.out.println(course);
            }
        }catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler bei Kurs-Anzeigen für laufende: " + databaseException.getMessage());
        }catch (Exception exception){
            System.out.println("Unbekannter Fehler bei Kurs-Anzeigen für laufende Kurse: " + exception.getMessage());
        }
    }

    private void courseSearch() {
        System.out.println("Geben Sie einen Suchbegriff an!");
        String searchString = scanner.nextLine();
        List<Course> courseList;
        try {
            courseList = repo.findAllCoursesByNameOrDescription(searchString);
            for (Course course: courseList) {
                System.out.println(course);
            }
        }catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler bei der Kurssuche: " + databaseException.getMessage());
        }catch (Exception exception){
            System.out.println("Unbekannter Fehler bei der Kurssuche: " + exception.getMessage());
        }
    }

    private void deleteCourse() {

        System.out.println("Welchen Kurs möchten Sie löschen? Bitte ID eingeben: ");

        try {
            Long courseIdToDelete = Long.parseLong(scanner.nextLine());
            if(repo.deleteById(courseIdToDelete) == true){
                System.out.println("Kurs mit ID " + courseIdToDelete + " gelöscht!");
            }else {
                System.out.println("Kurs wurde nicht gelöscht");
            }
        }catch (NumberFormatException numberFormatException){
            System.out.println("Unbekanntes Format: " + numberFormatException);
        }catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler beim Löschen: " + databaseException.getMessage());
        }
        catch (Exception exception){
            System.out.println("Unbekannterfehler beim Löschen: " + exception.getMessage());
        }
    }

    private void updateCourseDetails() {
        System.out.println("Für welche Kurs-ID möchten Sie die Kursdetails ändern?");
        Long courseId = Long.parseLong(scanner.nextLine());

        try {
            Optional<Course> optionalCourse = repo.getById(courseId);
            if(optionalCourse.isEmpty()){
                System.out.println("Kurs mit der gegebenen ID nicht in der Datenbank");
            }else{
                Course course = optionalCourse.get();

                System.out.println("Änderungen für folgenden Kurs");
                System.out.println(course);

                String name, description, hours, dateFrom, dateTo, courseType;

                System.out.println("Bitte neue Kursdaten angeben (Enter, falls keine Änderung gewünscht ist): ");
                System.out.println("Name: ");
                name = scanner.nextLine();
                System.out.println("Beschreibung: ");
                description = scanner.nextLine();
                System.out.println("Stundenanzahl: ");
                hours = scanner.nextLine();
                System.out.println("Startdatum (YYYY-MM-DD): ");
                dateFrom = scanner.nextLine();
                System.out.println("Enddatum (YYYY-MM-DD): ");
                dateTo = scanner.nextLine();
                System.out.println("Kurstyp (ZA/BF/FF/OE): ");
                courseType = scanner.nextLine();

                Optional<Course> courseOptionalUpdated = repo.update(
                        new Course(
                                course.getId(),
                                name.equals("") ? course.getName() : name,
                                description.equals("") ? course.getDescription() : description,
                                hours.equals("") ? course.getHours() : Integer.parseInt(hours),
                                dateFrom.equals("") ? course.getBeginDate() : Date.valueOf(dateFrom),
                                dateTo.equals("") ? course.getEndDate() : Date.valueOf(dateTo),
                                courseType.equals("") ? course.getCourseTyp() : CourseTyp.valueOf(courseType)
                        )
                );
                courseOptionalUpdated.ifPresentOrElse(
                        (c)-> System.out.println("Kurs aktualisiert: " + c),
                        ()-> System.out.println("Kurs konnte nicht aktualisiert werden!")
                );
            }
        }catch (IllegalArgumentException illegalArgumentException){
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
        }catch (InvalidValueException invalidValueException){
            System.out.println("Kursdaten nicht korrekt angegeben: " + invalidValueException.getMessage());
        }catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler beim Einfügen: " + databaseException.getMessage());
        }catch (Exception exception){
            System.out.println("Unbekannter Fehler beim Einfügen: " + exception.getMessage());
        }
    }

    private void addCourse() {
        String name, description;
        int hours;
        Date dateFrom, dateTo;
        CourseTyp courseTyp;

        try {
            System.out.println("Bitte alle Kursdaten angeben:");
            System.out.println("Name: ");
            name = scanner.nextLine();
            if(name.equals("")) throw new IllegalArgumentException("Eingabe darf nicht leer sein!");
            System.out.println("Beschreibung: ");
            description = scanner.nextLine();
            if(description.equals("")) throw new IllegalArgumentException("Eingabe darf nicht leer sein!");
            System.out.println("Stundenanzahl: ");
            hours = Integer.parseInt(scanner.nextLine());
            System.out.println("Startdatum (YYYY-MM-DD): ");
            dateFrom = Date.valueOf(scanner.nextLine());
            System.out.println("Enddatum (YYYY-MM-DD): ");
            dateTo = Date.valueOf(scanner.nextLine());
            System.out.println("Kurstyp: (ZA/BF/FF/OE): ");
            courseTyp = CourseTyp.valueOf(scanner.nextLine());

            Optional<Course> optionalCourse = repo.insert(new Course(
                    name, description, hours, dateFrom, dateTo, courseTyp));

            if (optionalCourse.isPresent()){
                System.out.println("Kurs angelegt: " + optionalCourse.get());
            }else{
                System.out.println("Kurs wurde nicht angelegt!");
            }


        }catch (IllegalArgumentException illegalArgumentException){
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
        }catch (InvalidValueException invalidValueException){
            System.out.println("Kursdaten nicht korrekt angegeben: " + invalidValueException.getMessage());
        }catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler beim Einfügen: " + databaseException.getMessage());
        }catch (Exception exception){
            System.out.println("Unbekannter Fehler beim Einfügen: " + exception.getMessage());
        }
    }

    private void showCourseDetails() {
        System.out.println("Für welchen Kurs möchten Sie die Kursdetails anzeigen?");
        Long courseId = Long.parseLong(scanner.nextLine());
        try {
            Optional<Course> courseOptional = repo.getById(courseId);
            if(courseOptional.isPresent()){
                System.out.println(courseOptional.get());
            }else {
                System.out.println("Kurs mit der ID "+ courseId + " nicht gefunden!");
            }

        }catch (DatabaseException databaseException){
            System.out.println("Datenbankfehler bei Kurs-Detailanzeigen: " + databaseException.getMessage());
        }catch (Exception exception){
            System.out.println("Unbekannter Fehler bei Kurs-Detailsanzeigen: " + exception.getMessage());
        }
    }

    private void showAllCourses() {
        List<Course> listCourse = null;

        try {
            listCourse = repo.getAll();
            if (listCourse.size() > 0) {
                for (Course course : listCourse) {
                    System.out.println(course.toString());
                }
            } else {
                System.out.println("Kursliste leer!");
            }
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler bei Anzeigen aller Kurse: " + databaseException.getMessage());
        } catch (Exception exception){
            System.out.println("Unbekannter Fehler bei Anzeigen aller Kurse: " + exception.getMessage());
        }
    }

    public void start() {
        String input = "-";
        while (!input.equals("x")) {
            showMenue();
            input = scanner.nextLine();
            switch (input) {
                case "1" -> courseMenue();
                case "2" -> studentMenue();
                case "3" -> bookingMenue();
                case "x" -> System.out.println("Auf Wiedersehen");
                default -> inputError();
            }
        }
        scanner.close();
    }

    public void courseMenue() {
        String input = "-";
        while (!input.equals("x")) {
            showCourseMenue();
            input = scanner.nextLine();
            switch (input) {
                case "1" -> addCourse();
                case "2" -> showAllCourses();
                case "3" -> showCourseDetails();
                case "4" -> updateCourseDetails();
                case "5" -> deleteCourse();
                case "6" -> courseSearch();
                case "7" -> runningCourses();
                case "8" -> findPerName();
                case "9" -> findPerDescription();
                case "10" -> findPerstartDate();
                case "11" -> findPerCourseTyp();
                case "x" -> System.out.println("zurück ins Hauptmenü");
                default -> inputError();
            }
        }
    }

    public void studentMenue() {
        String input = "-";
        while (!input.equals("x")) {
            showStudentMenue();
            input = scanner.nextLine();
            switch (input) {
                case "1" -> addStudent();
                case "2" -> showAllStudents();
                case "3" -> showStudentDetails();
                case "4" -> updateStudent();
                case "5" -> deleteStudent();
                case "6" -> findStudentByFirstname();
                case "7" -> findStudentByLastname();
                case "8" -> findStudentByBirthdate();
                case "9" -> findStudentBetweenById();
                case "10" -> findStudentBetweenBithdate();
                case "x" -> System.out.println("Hauptmenü");
                default -> inputError();
            }
        }
    }
    public void bookingMenue() {
        String input = "-";
        while (!input.equals("x")) {
            showBookingMenue();
            input = scanner.nextLine();
            switch (input) {
                case "1" -> addBooking();
                case "2" -> getByIdbooking();
                case "3" -> showAllBookings();
                case "4" -> updateBooking();
                case "5" -> deleteBooking();
                case "6" -> findAllBookingsByCourseID();
                case "7" -> findAllBookingsByStudentID();
                case "8" -> findAllBookingsByDate();
                case "9" -> findAllStudentsInCourseWithInformation();
                case "10" -> findAllCoursesByStudentWithInformation();
                case "x" -> System.out.println("Hauptmenü");
                default -> inputError();
            }
        }
    }

    private void showMenue() {
        System.out.println("-----------HAUPTMENÜ-------------");
        System.out.println("(1) Kursmanagement \t (2) Studentmanagement \t (3) Buchungsmanagement");
        System.out.println("(x) Ende");

    }
    private void showCourseMenue() {
        System.out.println("--------------------------KURSMANAGEMENT COURSE-------------------");
        System.out.println("(1) Kurs eingeben \t (2) Alle Kurse anzeigen \t" + "(3) Kursdetails anzeigen");
        System.out.println("(4) Kursdetails ändern \t (5) Kurs löschen \t" + "(6) Kurssuche");
        System.out.println("(7) Laufende Kurse \t (8) Kurssuche mit Namen \t" + "(9) Kurssuche mit Beschreibung");
        System.out.println("(10) Kurssuche Startdatum \t (11) Kurssuche mit Kurstyp \t");
    }
    private void showStudentMenue() {
        System.out.println("--------------------------KURSMANAGEMENT Student-------------------");
        System.out.println("(1) Studenten eingeben \t (2) Alle Studenten anzeigen \t" + "(3) Studentendetails anzeigen");
        System.out.println("(4) Studentendetails ändern \t (5) Student löschen \t" + "(6) Student suche nach Vorname");
        System.out.println("(7) Student suche nach Nachnamen \t (8) Student suche nach Geburtstag \t" + "(9) Studenten suchen nach ID");
        System.out.println("(10) Student suche zwischen Geburtstag \t");
    }
    private void showBookingMenue() {
        System.out.println("--------------------------KURSMANAGEMENT BOOKING-------------------");
        System.out.println("(1) Buchung eingeben \t (2) Buchungsdetails anzeigen\t" + "(3) Buchungen anzeigen");
        System.out.println("(4) Buchung aktualisieren \t (5) Buchung löschen\t" + "(6) Alle Buchungen mit per Kurs-ID");
        System.out.println("(7) Alle Buchungen mit per Student-ID \t (8) Alle Buchungen per Datum\t" + "(9) Studenten im Kurs");
        System.out.println("(10) Kursdetails bei Student");
        System.out.println("(x) ENDE");
    }


    private void inputError(){
        System.out.println("Bitte nur die Zahl der Menüauswahl eingeben!");
    }
}
